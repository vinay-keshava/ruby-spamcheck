module gitlab.com/gitlab-org/spamcheck

require (
	github.com/golang/protobuf v1.4.3
	github.com/grpc-ecosystem/go-grpc-middleware v1.0.0
	github.com/grpc-ecosystem/go-grpc-prometheus v1.2.0
	github.com/grpc-ecosystem/grpc-gateway/v2 v2.1.0
	github.com/pelletier/go-toml v1.8.1
	github.com/stretchr/testify v1.6.1
	gitlab.com/gitlab-org/labkit v1.3.0
	golang.org/x/tools v0.1.0 // indirect
	google.golang.org/genproto v0.0.0-20210106152847-07624b53cd92
	google.golang.org/grpc v1.34.0
	google.golang.org/grpc/cmd/protoc-gen-go-grpc v1.0.1
	google.golang.org/protobuf v1.25.0
)

go 1.15
