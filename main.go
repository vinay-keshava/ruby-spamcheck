package main

import (
	"gitlab.com/gitlab-org/spamcheck/app"
)

func main() {
	app := createApplication()
	app.Run()
}

func createApplication() *app.SpamCheck {
	return &app.SpamCheck{}
}
