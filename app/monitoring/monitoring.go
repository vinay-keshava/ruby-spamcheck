package monitoring

import (
	"gitlab.com/gitlab-org/labkit/log"
	"gitlab.com/gitlab-org/labkit/monitoring"
	config "gitlab.com/gitlab-org/spamcheck/app/config"
)

var err error

func LoadMonitoring(cfg *config.Config) error {
	// Initialize LabKit Monitoring
	go func(){
		err = monitoring.Start(
			monitoring.WithListenerAddress(cfg.Monitor.Address),
			monitoring.WithBuildInformation("0.0.1", "2021-01-08T14:20:00Z"),
		)
	}()

	if err == nil {
		log.Info("Monitoring started")
	}
	return err	 
}
