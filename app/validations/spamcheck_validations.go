package validations

import (
	"context"
	"errors"

	spamcheck "gitlab.com/gitlab-org/spamcheck/api/v1"
)

type SpamCheckValidations interface {
	ValidateIssue(ctx context.Context, spammable *spamcheck.Issue) error
}

type spamCheckValidations struct {
}

// CreateSpamCheckValidations contsructs the validation object
func CreateSpamCheckValidations() SpamCheckValidations {
	return new(spamCheckValidations)
}

// ValidateIssue Returns errors if the Issue is not valid
func (s *spamCheckValidations) ValidateIssue(ctx context.Context, issue *spamcheck.Issue) error {
	if len(issue.Title) == 0 {
		return errors.New("Issue title is required")
	}

	return nil
}
