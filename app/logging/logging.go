package logging

import (
	"gitlab.com/gitlab-org/labkit/log"
	config "gitlab.com/gitlab-org/spamcheck/app/config"
)

func LoadLogging(cfg *config.Config) error {
	// Initialize LabKit Logger
	logCloser, err := log.Initialize(
		log.WithFormatter(cfg.Logger.Format),
		log.WithLogLevel(cfg.Logger.Level),
		log.WithOutputName(cfg.Logger.Output),
	)
	defer logCloser.Close()

	if err == nil {
		log.Info("Logger started")
	}
	return err
}
